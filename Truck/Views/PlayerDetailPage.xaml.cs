﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;

using Microsoft.Toolkit.Uwp.UI.Animations;
using Newtonsoft.Json.Linq;
using Truck.Services;

using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

namespace Truck.Views
{
    public sealed partial class PlayerDetailPage : Page, INotifyPropertyChanged
    {
       

        public PlayerDetailPage()
        {
            InitializeComponent();
     
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            if (e.Parameter is long orderId)
            {
               
            }
        }

        protected override void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
            base.OnNavigatingFrom(e);
            if (e.NavigationMode == NavigationMode.Back)
            {
                
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void Set<T>(ref T storage, T value, [CallerMemberName]string propertyName = null)
        {
            if (Equals(storage, value))
            {
                return;
            }

            storage = value;
            OnPropertyChanged(propertyName);
        }

        private void OnPropertyChanged(string propertyName) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        private void Player_id_SelectionChanged(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
           
            
        }

        private async void Look_player_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            {
                StreamReader reader = default(StreamReader);
                //player lookup
                string url = "https://api.truckersmp.com/v2/player/" + player_name.Text;

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                HttpWebResponse response = (HttpWebResponse)await request.GetResponseAsync(); // (note: await added, method call changed to GetResponseAsync()

                reader = new StreamReader(response.GetResponseStream());
                string responseFromServer = reader.ReadToEnd();
                string json = responseFromServer;
                string rawresp;
                rawresp = reader.ReadToEnd();
                JObject ser = (JObject)(JObject.Parse(json));
                List<JToken> data = ser.Children().ToList();
                string output = "";

                foreach (JProperty item in data)
                {
                    item.CreateReader();
                    switch (item.Name)
                    {
                        case "response":
                            output += Convert.ToString("" + "\r\n");
                            foreach (JObject stream in item)
                            {

                                string name = (string)(stream["name"]); // username
                                string steamID = (string)(stream["steamID64"]); // steamID for that player
                                string JoinDate = (string)(stream["JoinDate"]); // JoinDate
                                string groupName = (string)(stream["groupName"]); // Groupname
                                string DisplayBan = (string)(stream["displayBans"]); // Tell us rather if users has their ban hidden
                                string id = (string)(stream["id"]); // player ID
                                Ban_search_Click(null, null); // click ban search button

                                //list_bans.Items.Add( "Username" + " " + name);
                                //list_bans.Items.Add("ID:" + "" + id);
                                // list_bans.Items.Add("JoinDate:" + "" + JoinDate);
                                //search_user.Text = name;
                                name_block.Text = "Username: " + name; // username of banned player
                                player_id.Text = "Player ID: " + id; // Player ID
                                group_name.Text = "Group: " + groupName; // Group name assigned to admin/player

                                // Display text showing us if player has his/her ban history set to private
                                if ((DisplayBan == "False"))
                                {
                                    player_history.Items.Add("User has chosen to hide their history"); 
                                }


                                // list.Items.Add(name + " " + game + " " + players + "/" + max);

                                //list.Items.Add (list.Items.Count);
                                //textme.Text = ("Total Servers Online: ") + " " + list.Items.Count.ToString();

                            }
                            break;


                    }
                }
            }
        }

        private async void Ban_search_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            StreamReader reader = default(StreamReader);
            //player lookup
            string url = "https://api.truckersmp.com/v2/bans/" + player_name.Text;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            HttpWebResponse response = (HttpWebResponse)await request.GetResponseAsync(); // (note: await added, method call changed to GetResponseAsync()

            reader = new StreamReader(response.GetResponseStream());
            string responseFromServer = reader.ReadToEnd();
            string json = responseFromServer;
            string rawresp;
            rawresp = reader.ReadToEnd();
            JObject ser = (JObject)(JObject.Parse(json));
            List<JToken> data = ser.Children().ToList();
            string output = "";

            foreach (JProperty item in data)
            {
                item.CreateReader();
                switch (item.Name)
                {
                    case "response":
                        output += Convert.ToString("" + "\r\n");
                        foreach (JObject stream in item.Value)
                        {

                            string name = (string)(stream["adminName"]);
                            string reason = (string)(stream["reason"]);
                            //string JoinDate = (string)(stream["JoinDate"]);


                            string time = (string)(stream["timeAdded"]);


                            player_history.Items.Add("AdminName" + " " + name); // Admin that banned user
                            player_history.Items.Add("Reason:" + "" + reason); // Reason they where banned
                            player_history.Items.Add("TimeAdded: " + "" + time); // time they where banned
                            // list_bans.Items.Add("JoinDate:" + "" + JoinDate);
                            //search_user.Text = id;

                            if ((player_name.Text == "NULL"))
                            {
                                //textme.Text = name + " " + " Server Offline";
                                var Msg = new Windows.UI.Popups.MessageDialog("Sorry we can not search a blank username try again");
                                await Msg.ShowAsync();
                            }


                            // list.Items.Add(name + " " + game + " " + players + "/" + max);

                            //list.Items.Add (list.Items.Count);
                            //textme.Text = ("Total Servers Online: ") + " " + list.Items.Count.ToString();

                        }
                        break;


                }
            }
        }

        private void Clear_list_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            player_history.Items.Clear();
        }
    }
}
   

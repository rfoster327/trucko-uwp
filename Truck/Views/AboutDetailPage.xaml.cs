﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

using Microsoft.Toolkit.Uwp.UI.Animations;
using Newtonsoft.Json;
using Truck.Services;
using System.Reflection;
using System.Diagnostics;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

namespace Truck.Views
{
    public sealed partial class AboutDetailPage : Page, INotifyPropertyChanged
    {
      

        public AboutDetailPage()
        {
            InitializeComponent();
            Cv_press_Click(null, null);
           
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            if (e.Parameter is long orderId)
            {
                
            }
        }

        protected override void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
            base.OnNavigatingFrom(e);
            if (e.NavigationMode == NavigationMode.Back)
            {
                
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void Set<T>(ref T storage, T value, [CallerMemberName]string propertyName = null)
        {
            if (Equals(storage, value))
            {
                return;
            }

            storage = value;
            OnPropertyChanged(propertyName);
        }

        private void OnPropertyChanged(string propertyName) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        private void Cv_press_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e) // button function that calls the app updated information
        {
            using (var webClient = new System.Net.WebClient())
            {
                var json = webClient.DownloadString("https://api.truckersmp.com/v2/version"); // URL to truckersMP API for grabbing version information do not change this URL 
                dynamic jarray = JsonConvert.DeserializeObject(json); // covert json
                var ets_version = jarray.supported_game_version.ToString(); // get current ETS 2 Version
                var ats_version = jarray.supported_ats_game_version.ToString(); // get current ATS Version
                var truckers_mp = jarray.name.ToString(); // get current TruckersMP Version that supported
                var thisApp = Assembly.GetExecutingAssembly();
                AssemblyName name = new AssemblyName(thisApp.FullName);
                string VersionNumber = "Alpha Version: " + name.Version; // get current version of TruckBeam app
                // Prints out current supported ATS version, Current MP Version, and Current ETS2 supported game version
                cv_text.Text = "ETS Version:" + ets_version  + " " + Environment.NewLine + "ATS Version: " +  ats_version + Environment.NewLine + "MP Version: " + truckers_mp + " " + VersionNumber; 
            }

        }

        private void TextBlock_SelectionChanged(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {

        }

        private void TextBlock_SelectionChanged_1(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {

        }
    }
        }

    


﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using Newtonsoft.Json.Linq;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Truck.Views
{
    public sealed partial class ServerPage : Page, INotifyPropertyChanged
    {
        private int counter = 0; // int for timer
        private DispatcherTimer timer; // Dispatch timer
        public ServerPage()
        {
            InitializeComponent();
            timer = new DispatcherTimer(); // start a new timer on load
            Start_Click(null, null); // Button that calls the start to load server on main form load
            timer.Interval = TimeSpan.FromSeconds(1.0); // timer interval to update server listbox
            timer.Tick += Timer_Tick; // timer tick
            //textme.Text = counter.ToString();
            timer.Start(); // start
           

        }
        // timer tick function do not change anything on this line as it can break the server load status.
        private void Timer_Tick(object sender, object e)
        {
            if (counter == 60)
            {
                server.Items.Clear(); // clear server list on refresh to ensure we do not spam server list box with the same information over and over.
                Start_Click(null, null); // a method of calling a button click since C# doesn't support VB.net methods
                counter = 0;
                // textme.Text = counter.ToString();
                //if you want to stop the timer use timer.Start()
            }
            else
            {
                counter++;
            }




        }
        public event PropertyChangedEventHandler PropertyChanged;

        private void Set<T>(ref T storage, T value, [CallerMemberName]string propertyName = null)
        {
            if (Equals(storage, value))
            {
                return;
            }

            storage = value;
            OnPropertyChanged(propertyName);
        }

        private void OnPropertyChanged(string propertyName) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        // TODO WTS: Do not change the URL of TruckersMP API
        private async void Start_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {


            StreamReader reader = default(StreamReader);

            string url = "https://api.truckersmp.com/v2/servers"; // TruckersMP API URL
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            HttpWebResponse response = (HttpWebResponse)await request.GetResponseAsync(); // (note: await added, method call changed to GetResponseAsync()

            reader = new StreamReader(response.GetResponseStream());
            string responseFromServer = reader.ReadToEnd();
            string json = responseFromServer;
            string rawresp;
            rawresp = reader.ReadToEnd();
            JObject ser = (JObject)(JObject.Parse(json));
            List<JToken> data = ser.Children().ToList();
            string output = "";

            foreach (JProperty item in data)
            {
                item.CreateReader();
                switch (item.Name)
                {
                    case "response":
                        output += System.Convert.ToString("" + "\r\n");
                        foreach (JObject stream in item.Values())
                        {
                            string game = (string)(stream["game"]); //Display current game whether it ATS or ETS2
                            string name = (string)(stream["name"]); //Display current name
                            string players = (string)(stream["players"]); // Display total players online
                            string max = (string)(stream["maxplayers"]); // Display total max players has set
                            string online = (string)(stream["online"]); // Display current server status
                            string id = (string)(stream["id"]); // Display the Server ID for server
                            string shortname = (string)(stream["shortname"]); // Display shortname for servers
                            {

                            }

                            // if server appears offline add it to server list still but marked it has offline
                            if ((name == "False")) 
                            {
                                server.Items.Add(name + " " + game + " " + players + "/" + max + "Is Offline" );
                            }

                            else
                            
                            {
                                // Display the servers currently online with server detail information
                                server.Items.Add(name + " " + game + " " + players + "/" + max  );

                            }

                           


                            // if an event server is online let show a message to them
                            if ((shortname == "EVENT"))
                            {

                                // event_update.Text = name + " " + "Event Server Is Online";


                            }

                            else
                            {

                                // if no event servers online let a users know

                                //event_update.Text = "No event servers are online";

                            }


                            //list.Items.Add (list.Items.Count);


                            // Display the total amount of servers truckersMP has even
                            //if offline
                            //textme.Text = ("Total Servers Online: ") + " " + list.Items.Count.ToString();

                            // if servers exist do not add them again
                            //if (server.Items.Contains(id))
                            //{

                            // }
                            //else
                            {
                                //     server.Items.Add(id);
                            }


                        }
                        break;


                }
            }
        }

    }
}
